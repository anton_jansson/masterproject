

import numpy
import OD4Session
import opendlv_standard_message_set_v0_9_10_pb2

#voltage = None


def convertIrVoltageToDistance(voltage):

	voltageDividerR1 = 1000;
	voltageDividerR2 = 1000;

	sensorVoltage = (voltageDividerR1 + voltageDividerR2) / voltageDividerR2 * voltage;
	distance = (2.5 - sensorVoltage) / 0.07;
	return distance;




distances = {"front":0.0, "rear":0.0}; 
voltage = {"right":0.0, "left":0.0}; 
groundSteering = 0
pedalPosition = 0


def onDistance(msg, senderStamp, timeStamps):
	
	if senderStamp == 0:
		distance["front"]= msg.distance
	
	if senderStamp == 1:
		distance["rear"] = msg.distance

def onVoltage(msg, senderStamp, timeStamps):
	
	if senderStamp == 0:
		distance["right"]= convertIrVoltageToDistance(msg.voltage)
	
	if senderStamp == 1:
		distance["left"] = convertIrVoltageToDistance(msg.voltage)



session = OD4Session.OD4Session(cid=112) #112 is cid for kiwi car

messageIDDistanceReading=1039
messageIDVoltageReading=1037

session.registerMessageCallback(messageIDDistanceReading,onDistance,opendlv_standard_message_set_v0_9_10_pb2.opendlv_proxy_DistanceReading) #Incoming signals
session.registerMessageCallback(messageIDVoltageReading,onVoltage,opendlv_standard_message_set_v0_9_10_pb2.opendlv_proxy_VoltageReading)

session.connect()

messageIdAngleRequest = 1084
messageIdPedalPositionRequest = 1086

while(True):
	if(distances["front"] < 0.2):
		pedalPosition = 0.1


	angleRequest = opendlv_standard_message_set_v0_9_10_pb2.opendlv_proxy_GroundSteeringRequest()
	pedalPositionRequest = opendlv_standard_message_set_v0_9_10_pb2.opendlv_proxy_PedalPositionRequest()

	angleRequest.groundSteering= groundSteering
	pedalPositionRequest.position = pedalPosition


	session.send(messageIdAngleRequest, angleRequest.SerializeToString())  
	session.send(messageIdPedalPositionRequest, pedalPositionRequest.SerializeToString())




